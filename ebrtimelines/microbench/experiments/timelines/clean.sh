#!/bin/bash

rm -rf debra/data
rm -rf tokens/data
rm -rf ../../../../plots/generated_plots/plot_Fig6,7,8,9
rm -rf ../../../../plots/generated_plots/plot_Fig2
rm -rf ../../../../plots/generated_plots/plot_Fig3,4

# rm -rf data_ll
# rm -rf data_hl
# rm -rf data_hm
# rm -rf data_hmht
# rm -rf data_dgt
# rm -rf data_abtree
#rm -rf data_dgttd
# rm -rf data_dgtntd

# rm -rf __pycache__
# rm -rf plots/plot_data_*
