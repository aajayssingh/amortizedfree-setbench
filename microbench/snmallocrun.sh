#!/bin/sh
#script to run snmalloc related tests

ac=snmallocshimPostPR; #other options: snmallocshimPrePR mimalloc snmallocshimPostPR
size=20000000;insdel=50;ds=brown_ext_abtree_lf;dur=5000;

cols="%12s %12s %12s %12s\n"; 
printf "$cols" alg nthreads tput max_res;
bin="bin"
started=`date`
for n in 144; do
    for alg in debra; do
        avgtput=0 ; 
        for trial in 1 2 3 4 5 6 7 8 9 10; do
            sfname=$bin/step_${alg}_${n}_${trial}.txt; 
            ex_cmd="LD_PRELOAD=../lib/lib${ac}.so time -f \"cmd:%C \nmemory:%M\" -o $bin/timetemp.txt numactl --interleave=all $bin/ubench_${ds}.alloc_new.reclaim_${alg}.pool_none.out -nwork $n -nprefill $n -i ${insdel} -d ${insdel} -rq 0 -rqsize 1 -k ${size} -nrq 0 -t ${dur}"
            #  -pin 0-23,96-119,24-47,120-143,48-71,144-167,72-95,168-191"
            
            # echo "eval $ex_cmd >> $f 2>&1"
            eval $ex_cmd > ${sfname} ;
            
            tput=`cat ${sfname} | grep "total throughput" | cut -d ":" -f2 | tr -d " "  `; 
            max_resmem=`cat $bin/timetemp.txt | grep memory | tail -1 | cut -d":" -f2`; 

            printf "%12s %12d %12d %12d\n" "$alg" "$n" "$tput" "$max_resmem" | tee $bin/templine.txt; cat $bin/templine.txt | tr -s ' ' | tr ' ' ',' | sed 's/^,//'| paste -sd " " >> $bin/data.csv;
        done; 
    done ;
    echo "-------------------";
done;

echo "started: $started"
echo "finished:" `date`

# alloc=mimalloc;ds=brown_ext_abtree_lf;rm data_jax.csv;cols="%12s %12s %12s %12s\n"; printf "$cols" alg nthreads tput max_res; for n in 144; do for alg in debra; do avgtput=0 ; for ((trial=0;trial<6;++trial)); do sfname=step_${alg}_${n}_${trial}.txt; [[ $n -gt 1 ]] && LD_PRELOAD=../../lib/lib${alloc}.so time -f "cmd:%C \nmemory:%M" -o timetemp.txt ./ubench_${ds}.alloc_new.reclaim_${alg}.pool_none.out -nwork $n -nprefill $n -i 50 -d 50 -rq 0 -rqsize 1 -k 20000000 -nrq 0 -t 5000 > ${sfname} || LD_PRELOAD=../../lib/libsnmallocshim.so time -f "cmd:%C \nmemory:%M" -o timetemp.txt numactl --interleave=all ./ubench_${ds}.alloc_new.reclaim_${alg}.pool_none.out -nwork $n -nprefill $n -i 50 -d 50 -rq 0 -rqsize 1 -k 20000000 -nrq 0 -t 5000 -pin 0-23,24-47,48-71,72-95,96-119,120-143,144-167,168-191 > ${sfname} ; tput=`cat ${sfname} | grep "total throughput" | cut -d ":" -f2 | tr -d " "  `; max_resmem=`cat timetemp.txt | grep memory | tail -1 | cut -d":" -f2` ; printf "%12s %12d %12d %12d\n" "$alg" "$n" "$tput" "$max_resmem" | tee templine.txt; cat templine.txt | paste -sd " " >> data_jax.csv; done; done ; echo "-------------------";done;
