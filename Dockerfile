FROM ubuntu:20.04
RUN apt update && apt install -y \
        build-essential \
        dos2unix \
        g++ \
        git \
        libnuma-dev \
        make \
        numactl \
        parallel \
        python3 \
        python3-pip \
        time \
        zip \
        bc \
        micro

RUN apt update && apt install -y \
        linux-tools-generic \
        linux-tools-common \
        linux-tools-5.8.0-55-generic \
        && echo alias perf=$(find / -wholename "*-generic/perf") > ~/.bash_aliases

RUN pip3 install \
        numpy \
        matplotlib \
        pandas \
        seaborn \
        ipython \
        ipykernel \
        jinja2 \
        colorama

COPY . /amortizedfree-setbench/
WORKDIR /amortizedfree-setbench/

CMD bash -C 'myscript.sh';'bash'
