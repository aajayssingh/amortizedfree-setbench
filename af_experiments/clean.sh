#!/bin/bash

rm output_*
rm -rf bin
# rm -rf data_ll
# rm -rf data_hl
# rm -rf data_hm
# rm -rf data_hmht
# rm -rf data_dgt
# rm -rf data_abtree
rm -rf data_dgttd
# rm -rf data_dgtntd

rm -rf __pycache__
rm -rf plots/generated_plots/*.*
