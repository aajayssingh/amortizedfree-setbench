#!/bin/bash

# experiment to see throughput varying for debra, debra amortized, token3 (periodic) and token4 (token amortized) over varying threads from 1 to 300 threads. Allocators used are jemalloc and snmalloc (PrePR and PostPR version).


# python3 ../tools/data_framework/run_experiment.py fig1_run.py -crdp

data_dir="data_e2"
exp_file=e2.py

python3 ../tools/data_framework/run_experiment.py $exp_file -crdp #-tr

# echo "copying FIGURES to plots/expected_plots/ "
# cp data/*.png plots/expected_plots/
mkdir plots/plot_$data_dir
echo "copying FIGURES to plots/plot_$data_dir/ "
cp $data_dir/*.png plots/plot_$data_dir/


# data_dir="data_e3"
# exp_file=e3.py

# python3 ../tools/data_framework/run_experiment.py $exp_file -rdp #-tr

# # echo "copying FIGURES to plots/expected_plots/ "
# # cp data/*.png plots/expected_plots/
# mkdir plots/plot_$data_dir
# echo "copying FIGURES to plots/plot_$data_dir/ "
# cp $data_dir/*.png plots/plot_$data_dir/