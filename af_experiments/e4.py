#!/usr/bin/python3
from _basic_functions import *

# 1 to 300. Debra

def define_experiment(exp_dict, args):
    set_dir_compile ( exp_dict, os.getcwd() + '/../microbench' )   ## working directory where the compilation command should be executed
    set_dir_tools   ( exp_dict, os.getcwd() + '/../tools' )        ## directory where the prerequisite tools library is located
    set_dir_run     ( exp_dict, os.getcwd() + '/../microbench/bin' )     ## working directory where your program should be executed
    set_dir_data    ( exp_dict, os.getcwd() + '/data_e4' )    ## directory for data files produced by individual runs of your program
    ## note: the above can be accessed using functions get_dir_[...]

    set_cmd_compile  (exp_dict, './compile.sh')

    ##
    ## add parameters that you want your experiments to be run with.
    ## your program will be run once for each set of values in the CROSS PRODUCT of all parameters.
    ## (i.e., we will run your program with every combination of parameters)
    ##
    ## if you want to perform repeated trials of each experimental configuration, add a run_param called "__trial"
    ##     and specify a list of trial numbers (as below).
    ##
    ## (the run_param doesn't *need* to be called __trials exactly, but if it is called __trials exactly,
    ##     then extra sanity checks will be performed to verify, for example, that each data point in a graphical plot
    ##     represents the average of precisely as many experimental runs as there are entries in the __trials list.)
    ##

    add_run_param     ( exp_dict, 'alloc', ['jemalloc', 'snmallocshimPrePR', 'snmallocshimPostPR'] ) #['jemalloc', 'tcmalloc', 'hoard', 'mimalloc', 'glibc', 'snmallocshim']
    add_run_param     ( exp_dict, 'INS_DEL_HALF'    , [50] )
    add_run_param     ( exp_dict, 'DS_SIZE'          , [20000000] )
    add_run_param     ( exp_dict, 'DS_TYPENAME'     , ['brown_ext_abtree_lf'] )
    add_run_param     (exp_dict, 'RECLAIMER_ALGOS', ['debra']) #['debra_df', 'debra', 'token3', 'token4'])
    add_run_param     ( exp_dict, 'MILLIS_TO_RUN'   , [5000])
    # add_run_param     ( exp_dict, 'thread_pinning'  , ['-pin ' + shell_to_str('cd ' + get_dir_tools(exp_dict) + ' ; ./get_pinning_cluster.sh', exit_on_error=True)] )
    add_run_param     ( exp_dict, '__trials'        , [1] )
    # add_run_param     ( exp_dict, 'TOTAL_THREADS'   , shell_to_listi('cd ' + get_dir_tools(exp_dict) + ' ; ./get_thread_counts_numa_nodes.sh', exit_on_error=True) )
    add_run_param     ( exp_dict, 'TOTAL_THREADS'   , [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300] )

# [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300]
    # list of numbers from 1 to 300: with a step 1.
# [48, 64, 96, 128, 144, 192, 240, 256, 280, 384]

    ## i like to have a testing mode (enabled with argument --testing) that runs for less time,
    ##  with fewer parameters (to make sure nothing will blow up before i run for 12 hours...)
    if args.testing:
        add_run_param ( exp_dict, '__trials'        , [1] )
        add_run_param ( exp_dict, 'TOTAL_THREADS'   , [24] )

    ##
    ## specify how to compile and run your program.
    ##
    ## you can use any of the parameters you defined above to dynamically replace {_tokens_like_this}.
    ## you can also get the directories saved above by using:
    ##      {__dir_compile}
    ##      {__dir_tools}
    ##      {__dir_run}
    ##      {__dir_data}
    ##
    ## the following replacement token is also defined for you:
    ##      {__step}            the number of runs done so far, padded to six digits with leading zeros
    ##
    ## your compile command will be executed in the compile directory above.
    ## your run command will be executed in the run directory above.
    ##

    # set_cmd_compile     ( exp_dict, 'make bin_dir={__dir_run} -j' )

    if args.testing:
        # set_cmd_run     ( exp_dict, 'LD_PRELOAD=../../../lib/lib{alloc}.so timeout 300 numactl --interleave=all time ./{DS_TYPENAME}.debra -nwork {TOTAL_THREADS} -insdel {INS_DEL_FRAC} -k {MAXKEY} -t 100 {thread_pinning} -rq 0 -rqsize 1 -nrq 0' )
        set_cmd_run (exp_dict, 'LD_PRELOAD=../../lib/lib{alloc}.so timeout 300 numactl --interleave=all time ./ubench_{DS_TYPENAME}.alloc_new.reclaim_{RECLAIMER_ALGOS}.pool_none.out -nwork {TOTAL_THREADS} -nprefill {TOTAL_THREADS} -i {INS_DEL_HALF} -d {INS_DEL_HALF} -rq 0 -rqsize 1 -k {DS_SIZE} -t 100')        
    else:
        # set_cmd_run     ( exp_dict, 'LD_PRELOAD=../../../lib/lib{alloc}.so timeout 300 numactl --interleave=all time ./{DS_TYPENAME}.debra -nwork {TOTAL_THREADS} -insdel {INS_DEL_FRAC} -k {MAXKEY} -t {MILLIS_TO_RUN} {thread_pinning} -rq 0 -rqsize 1 -nrq 0' )

        set_cmd_run (exp_dict, 'LD_PRELOAD=../../lib/lib{alloc}.so timeout 300 numactl --interleave=all time ./ubench_{DS_TYPENAME}.alloc_new.reclaim_{RECLAIMER_ALGOS}.pool_none.out -nwork {TOTAL_THREADS} -nprefill {TOTAL_THREADS} -i {INS_DEL_HALF} -d {INS_DEL_HALF} -rq 0 -rqsize 1 -k {DS_SIZE} -t 5000')

    ## pattern for output filenames. note 1: these files will be placed in {__dir_data}/. note 2: filenames cannot contain spaces.
    # set_file_data   ( exp_dict, 'data{__step}.txt' )

    ##
    ## add data fields to be fetched from all output files.
    ## each of these becomes a column in a data table in a sqlite database.
    ##
    ## by default, a field "XYZ" will be fetched from each data file using extractor grep_line,
    ##      which greps (searches) for a line of the form "XYZ=[arbitrary string]"
    ##
    ## if your field is not stored in that format, you can specify a custom "extractor" function,
    ##      as we do in our example "get_maxres" BELOW, to extract the max resident size
    ##      from the 6th space-separated column of the output of the linux "time" command
    ##
    ## also note: each of these fields becomes a replacement token, e.g., {PAPI_L3_TCM}.
    ##
    ## the following special fields are defined for you and automatically added to the resulting sqlite data table:
    ##      {__step}            the number of runs done so far, padded to six digits with leading zeros
    ##      {__cmd_run}         your cmd_run string with any tokens replaced appropriately for this run
    ##      {__file_data}       the output filename for the current run's data
    ##      {__path_data}       the relative path to the output file for the current run's data
    ##      {__hostname}        the result of running the hostname command on the machine
    ##

    ## note: in the following, defaults are "validator=is_nonempty" and "extractor=grep_line"

    add_data_field ( exp_dict, 'total_throughput'  , coltype='INTEGER' , validator=is_positive )
    # add_data_field ( exp_dict, 'PAPI_L3_TCM'       , coltype='REAL' )
    # add_data_field ( exp_dict, 'PAPI_L2_TCM'       , coltype='REAL' )
    # add_data_field ( exp_dict, 'PAPI_TOT_CYC'      , coltype='REAL' )
    # add_data_field ( exp_dict, 'PAPI_TOT_INS'      , coltype='REAL' )
    add_data_field ( exp_dict, 'maxresident_mb'    , coltype='REAL'    , validator=is_positive , extractor=get_maxres ) ## note the custom extractor

    # add_data_field ( exp_dict, 'tree_stats_height' , coltype='INTEGER' )
    # add_data_field ( exp_dict, 'validate_result'   , coltype='TEXT'    , validator=is_equal('success') )
    # add_data_field ( exp_dict, 'MILLIS_TO_RUN'     , coltype='TEXT'    , validator=is_positive )
    # add_data_field ( exp_dict, 'RECLAIM'           , coltype='TEXT' )
    # add_data_field ( exp_dict, 'POOL'              , coltype='TEXT' )
    add_data_field   (exp_dict, 'alloc', validator=is_run_param('alloc'))

    ##
    ## add_plot_set() will cause a SET of plots to be rendered as images in the data directory.
    ##
    ## the precise SET of plots is defined by the fields included in varying_cols_list[].
    ##  we will iterate over all distinct combinations of values in varying_cols_list,
    ##  and will render a plot for each.
    ##
    ## note: a plot's title and filename can only use replacement {tokens} that correspond
    ##       to fields THAT ARE INCLUDED in varying_cols_list[]
    ##       (this is because only those tokens are well defined and unique PER PLOT)
    ##

    ## can do direct passthrough of command line args to the plotting script (see, e.g.,. ../../tools/plotbars.py to see what kind of customization of plots can be done this way)
    # plot_cmd_args = '--legend-include --legend-columns 4'

    ## you can also specify a python file containing hooks for configuring matplotlib's plotting style:
    ##    just after importing, and just before and after plotting.
    ## (your callback/hook functions will be provided with the mpl, plt, fig and ax variables
    ##    for matplotlib's environment, plot, figure and axes, so you can customize as you like.)

    # filters = [
    #   'DS_TYPENAME in ("brown_ext_abtree_lf", "bronson_pext_bst_occ")',
    # ]

    # for filter_str in filters:
    add_plot_set(
    exp_dict
    , name='throughput-{}-{}.png'.format('{DS_SIZE}', '{alloc}')
    , varying_cols_list=['DS_SIZE', 'alloc']
    , title='tput-{alloc} {INS_DEL_HALF} {DS_SIZE}'
    # , filter=filter_str
    , series='RECLAIMER_ALGOS'
    , x_axis='TOTAL_THREADS'
    , y_axis='total_throughput'
    , plot_type='line'
    # , plot_cmd_args='--legend-include'
    )

    # add_plot_set(
    # exp_dict
    # , name='maxresident-{}-{}.png'.format('{DS_SIZE}', '{alloc}')
    # , varying_cols_list=['DS_SIZE', 'alloc']
    # , title='MxResMB-{alloc} {INS_DEL_HALF} {DS_SIZE}'
    # # , filter=filter_str
    # , series='RECLAIMER_ALGOS'
    # , x_axis='TOTAL_THREADS'
    # , y_axis='maxresident_mb'
    # , plot_type='line'
    # # , plot_cmd_args='--legend-include'
    # )        

    # add_plot_set(
    #     exp_dict
    #     , name='throughput-vs-threads-{DS_TYPENAME}-{RECLAIMER_ALGOS}.png'
    #     , varying_cols_list=['RECLAIMER_ALGOS']
    #     , title='Throughput vs thread count'
    #     # , filter=filter_str
    #     , series='alloc'
    #     , x_axis='TOTAL_THREADS'
    #     , y_axis='total_throughput'
    #     , plot_type='bars'
    #     , plot_cmd_args='--legend-include'
    # )


## extractor functions take as arguments: a file to load data from, and a field name
def get_maxres(exp_dict, file_name, field_name):
    ## manually parse the maximum resident size from the output of `time` and add it to the data file
    maxres_kb_str = shell_to_str('grep "maxres" {} | cut -d" " -f6 | cut -d"m" -f1'.format(file_name))
    return float(maxres_kb_str) / 1000
